package com.qf.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author wgy
 * @version v1.0
 * @project day45_git
 * @Date 2023/4/20 11:15
 */
public class Book {
    private int id;
    private String title;
    private Date publicDate;
    private String picture;
    private BigDecimal price;
    private String isbn;
}
