package com.qf.entity;

/**
 * @author wgy
 * @version v1.0
 * @project day45_git
 * @Date 2023/4/20 11:19
 */
public class Dog {
    private String breed;
    private int age;
    private String hobby;
    private String name;
}
